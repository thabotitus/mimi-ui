
'use strict';

var DISTRIBUTION_FOLDER = 'public';

// Core references for this to work
var gulp = require('gulp'),
  browserSync = require('browser-sync').create(),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  reload = browserSync.reload,
  clean = require('gulp-clean'),
  minify = require('gulp-minify'),
  concat = require('gulp-concat'),
  htmlmin = require('gulp-htmlmin'),
  zip = require('gulp-zip');

// var listing of files for dist build
var filesToCopy = [
  './src/images/**/*.*',
  './src/lib/**/*',
  './src/js/dependencies.min.js'
];

// Use for stand-alone autoprefixer
var gulpautoprefixer = require('gulp-autoprefixer');

// alternate vars if you want to use Postcss as a setup
var postcss = require('gulp-postcss'),
  autoprefixer = require('autoprefixer');

// Gulp task when using gulp-autoprefixer as a standalone process
gulp.task(
  'build:css',
  function (done) {
    gulp.src('./src/sass/{,*/}*.{scss,sass}')
      .pipe(sourcemaps.init())
      .pipe(sass({
        errLogToConsole: true,
        //alt options: nested, compact, compressed
        outputStyle: 'compressed'
      }))
      .pipe(gulpautoprefixer({
        browsers: ['last 4 versions'],
        cascade: false
      }))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('./src/css'))
      .pipe(reload({ stream: true }));
    done();
  });

  gulp.task(
    'build:js',
    function () {
      return gulp.src(['./src/js/modules/*.js', 
                       './src/js/main.js'])
        .pipe(concat('mimi.js'))
        .pipe(minify({
          mangle: true,
          noSource: true,
          ext: {
            min: '.min.js'
          },
        }))
        .pipe(gulp.dest('./src/js'));
    }
  );
  gulp.task(
    'build:js:dependencies',
    function () {
      return gulp.src([
        './src/js/vendor/jquery-3.3.1.min.js',
        './src/js/vendor/icheck.min.js',
        './src/js/vendor/jquery.selectric.js',
        './src/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js',
        './src/js/vendor/prism.js',
        './src/js/vendor/prism-normalize-whitespace.min.js'
        ])
        .pipe(concat('dependencies.js'))
        .pipe(minify({
          mangle: true,
          noSource: true,
          ext: {
            min: '.min.js'
          },
        }))
        .pipe(gulp.dest('./src/js'));
    }
  );

// Static Server + watching scss/html files
gulp.task(
  'serve',
  gulp.series('build:css','build:js', 'build:js:dependencies', function () {
    browserSync.init({
      server: "./src/",
      port: 8080
    });

    gulp.watch('./src/sass/{,*/}*.{scss,sass}', gulp.parallel('build:css'));
    gulp.watch("./src/**/*.html").on('change', browserSync.reload);
  }),
);

// // Sass watcher
gulp.task(
  'sass:watch',
  function () {
    gulp.watch('./src/sass/{,*/}*.{scss,sass}', gulp.series('build:css'));
  });

gulp.task(
  'clean',
  function () {
    return gulp.src([`${DISTRIBUTION_FOLDER}/*`], { read: false }).pipe(clean());
  });

gulp.task(
  'build:dist:js',
  function () {
    return gulp.src(['./src/js/modules/*.js', 
                     './src/js/main.js'])
      .pipe(concat('mimi.js'))
      .pipe(minify({
        mangle: true,
        noSource: true,
        ext: {
          min: '.min.js'
        },
      }))
      .pipe(gulp.dest(`${DISTRIBUTION_FOLDER}/js`));
  }
);

gulp.task(
  'build:dist:css',
  function (done) {
    gulp.src(['./src/sass/{,*/}*.{scss,sass}'])
      .pipe(sass({
        errLogToConsole: true,
        outputStyle: 'compressed'
      }))
      .pipe(gulpautoprefixer({
        browsers: ['last 4 versions'],
        cascade: false
      }))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest(`./${DISTRIBUTION_FOLDER}/css`))
      .pipe(reload({ stream: true }));
    done();
  }
);

gulp.task('build:dist:html',
  function(done){
    gulp.src(['./src/*.html'])
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(`${DISTRIBUTION_FOLDER}`));
    gulp.src(['./src/demos/**/*.html'])
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(`${DISTRIBUTION_FOLDER}/demos`));
    done();
  });

gulp.task('build:dist:docs',
  function(done){
    gulp.src(['./src/docs/*.html'])
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(`${DISTRIBUTION_FOLDER}/docs`));
    done();
  });

gulp.task('build:dist:html:pages',
  function(done){
    gulp.src(['./src/pages/*.html'])
        .pipe(htmlmin({ collapseWhitespace: true }))
        .pipe(gulp.dest(`${DISTRIBUTION_FOLDER}/pages`));
    done();
  });

gulp.task('build:dist:copy',
  function(done){
    gulp.src(filesToCopy, { base: './src/' }).pipe(gulp.dest(`${DISTRIBUTION_FOLDER}`));
    done();
  });

gulp.task('build:dist:package', function(done) {
  gulp.src([`./${DISTRIBUTION_FOLDER}/css/`, `./${DISTRIBUTION_FOLDER}/js/`])
      .pipe(zip('MIMIv2.zip'))
      .pipe(gulp.dest('./package'));
  done();
});

// dist build tasks
// see var filesToDist for specific files
gulp.task(
  'build:dist',
  gulp.series(
    'clean', 
    gulp.parallel(
      'build:dist:copy', 
      'build:dist:js',
      'build:dist:css',
      'build:dist:html', 
      'build:dist:html:pages',
      'build:dist:docs'              
    ),
    'build:dist:package'
  ),
);

gulp.task(
  'default',
  gulp.series('build:css', 'sass:watch', 'serve'));
