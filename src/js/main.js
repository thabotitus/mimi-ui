/* eslint-disable */

(function () {
  'use strict';
  $(document).ready(function () {
    $.mimiUI.offcanvas.initialize();
    $.mimiUI.tabs.initialize();
    $.mimiUI.todo.initialize();
    $.mimiUI.dropdown.initialize();
    $.mimiUI.accordion.initialize();
    $.mimiUI.modal.initialize();
    $.mimiUI.select.initialize();
    $.mimiUI.checkbox.initialize();
  });
})();
