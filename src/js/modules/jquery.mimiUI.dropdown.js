'use strict';
(function($) {
  $.mimiUI = $.mimiUI || {};
  $.mimiUI.dropdown = {
    applyClickHandler: function() {
      var $triggers = $('[data-dropdown-trigger]');
      $.each($triggers, function() {
        $(this).on('click', function() {
          if ($(this).data('dropdown-active') === true) {
            $.mimiUI.dropdown._hideDropdown($(this));
            return false;
          }

          var $triggerEl = $(this).data('dropdown');
          var $matchinEl = $('body').find('[data-dropdown=' + $triggerEl + ']')
                                    .not($(this));
          $('[data-dropdown=""]').not($(this)).hide();
          $.mimiUI.dropdown._revealDropdown($matchinEl, $(this));
        });
      });
    },

    _revealDropdown: function($el, $trigger) {
      var $triggerPosition = $trigger.position();
      var $triggerHeight = $trigger.outerHeight();
      $trigger.data('dropdown-active', true);

      $el.css('left', $triggerPosition.left + 10);
      $el.css('top', $triggerPosition.top + $triggerHeight + 10);
      $el.fadeIn();
    },

    _hideDropdown: function($trigger) {
      var $triggerEl = $trigger.data('dropdown');
      var $matchinEl = $('body').find('[data-dropdown=' + $triggerEl + ']')
                                .not($trigger);

      $trigger.data('dropdown-active', false);
      $matchinEl.fadeOut();
    },

    /**
    * Initialize module
    */
    initialize: function() {
      this.applyClickHandler();
    }
  };
})(jQuery);
