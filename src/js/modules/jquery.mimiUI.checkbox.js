'use strict';
(function($) {
  $.mimiUI = $.mimiUI || {};
  $.mimiUI.checkbox = {
    applyCheckbox: function() {
      $('.mm-radio, .mm-checkbox').iCheck({
        checkboxClass: 'icheckbox_square',
        radioClass: 'iradio_square'
      });
    },
    /**
    * Initialize module
    */
    initialize: function() {
      this.applyCheckbox();
    }
  };
})(jQuery);
