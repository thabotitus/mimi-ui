'use strict';
(function($) {
  $.mimiUI = $.mimiUI || {};
  $.mimiUI.accordion = {
    initializeAll: function() {
      $('[data-accordion-content]').data('accordion-state', 'closed');
    },

    activeHeaderClass: function() {
      return 'mm-accorion__header--active';
    },

    applyClickHandler: function() {
      $('[data-accordion-header]').click(function() {
        var $state = $(this).next('[data-accordion-content]')
                            .data('accordion-state');
        var $contentToReveal = $(this).next('[data-accordion-content]');
        var $contentHeader = $(this);

        if ($state === 'open') {
          $.mimiUI.accordion._closeCurrentAccordion($(this));
          return;
        }

        $.mimiUI.accordion._closeAllAccordions($(this));
        $.mimiUI.accordion._removeActiveClass($(this));

        $contentHeader.addClass($.mimiUI.accordion.activeHeaderClass());
        $contentToReveal.data('accordion-state', 'open');
        $contentToReveal.slideDown('fast');
      });
    },

    _closeAllAccordions: function($currentEl) {
      var $parentAccordion = $currentEl.closest('[data-accordion]');
      var $content = $parentAccordion.find('[data-accordion-content]');

      $content.slideUp('fast');
      $content.data('accordion-state', 'closed');
    },

    _removeActiveClass: function($currentEl) {
      var $parentAccordion = $currentEl.closest('[data-accordion]');
      $parentAccordion.find('[data-accordion-header]')
                      .removeClass($.mimiUI.accordion.activeHeaderClass());
    },

    _closeCurrentAccordion: function($currentEl) {
      var $content = $currentEl.next('[data-accordion-content]');

      $content.slideUp('fast');
      $currentEl.removeClass($.mimiUI.accordion.activeHeaderClass());
      $content.data('accordion-state', 'closed');
    },

    /**
    * Initialize module
    */
    initialize: function() {
      this.initializeAll();
      this.applyClickHandler();
    }
  };
})(jQuery);
