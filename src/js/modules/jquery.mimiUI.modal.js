'use strict';
(function($) {
  $.mimiUI = $.mimiUI || {};
  $.mimiUI.modal = {
    overlayDiv: function() {
      return $('[data-modal-overlay]');
    },

    applyClickHandler: function() {
      $('[data-mm-modal-trigger]').click(function() {
        var $value = $(this).data('mm-modal-trigger');
        var matchingDiv = $('.mm-modal[data-modal=' + $value + ']');
        $.mimiUI.modal.overlayDiv().fadeIn('fast', function() {
          matchingDiv.fadeIn();
        });
      });
    },

    applyCloseClickHandler: function() {
      $('[data-close-modal]').click(function() {
        $.mimiUI.modal.closeOverlay();
      });
    },

    closeOverlay: function() {
      $('[data-modal]').hide();
      $.mimiUI.modal.overlayDiv().hide();
    },

    addOverlayDiv: function() {
      var overlayDiv = '<div class="mm-modal-overlay"' +
                       'data-modal-overlay></div>';
      $('body').append(overlayDiv);
    },

    applyClosingHandlerToOverlay: function() {
      $('[data-modal-overlay]').click(function() {
        $.mimiUI.modal.closeOverlay();
      });
    },
    /**
    * Initialize module
    */
    initialize: function() {
      this.applyClickHandler();
      this.applyCloseClickHandler();
      this.addOverlayDiv();
      this.applyClosingHandlerToOverlay();
    }
  };
})(jQuery);
