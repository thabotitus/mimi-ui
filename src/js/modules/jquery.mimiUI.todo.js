'use strict';
(function($) {
  $.mimiUI = $.mimiUI || {};
  $.mimiUI.todo = {
    applyClickHandler: function() {
      $.each($('[data-todo]'), function() {
        $(this).find('.mm-todo__item').on('click', function() {
          $.mimiUI.todo._completeTask($(this));
        });
      });
    },

    _completeTask: function(currentEl) {
      var $priority = $(currentEl).find('.mm-todo__priority');
      var successIcon = '<i class="ion-checkmark"></i>';

      if ($.mimiUI.todo._isComplete(currentEl)) {
        $.mimiUI.todo._uncompleteTask(currentEl);
        return;
      }

      $(currentEl).addClass('mm-todo__item--complete');
      $priority.empty();
      $priority.append(successIcon);
    },

    _isComplete: function(currentEl) {
      return currentEl.hasClass('mm-todo__item--complete');
    },

    _uncompleteTask: function(currentEl) {
      var $priority = $(currentEl).find('.mm-todo__priority');
      var highPriority = '<i class="ion-arrow-up-c"></i>';
      var normalPriority = '<i class="ion-minus-round"></i>';
      var lowPriority = '<i class="ion-arrow-down-c"></i>';

      $(currentEl).removeClass('mm-todo__item--complete');
      $priority.empty();
      switch ($priority.data('priority')) {
        case 'high':
          $priority.append(highPriority);
          break;
        case 'low':
          $priority.append(lowPriority);
          break;
        case 'normal':
          $priority.append(normalPriority);
          break;
        default:
          $priority.append(normalPriority);
      }
    },

    /**
    * Initialize module
    */
    initialize: function() {
      this.applyClickHandler();
    }
  };
})(jQuery);
