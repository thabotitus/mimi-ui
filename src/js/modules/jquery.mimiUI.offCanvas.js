'use strict';
(function($) {
  $.mimiUI = $.mimiUI || {};
  $.mimiUI.offcanvas = {
    applyClickHandler: function() {
      $('[data-mm-offcanvas-trigger]').click(function() {
        var offcanvasActive = $('body').attr('data-offcanvas');
        if (offcanvasActive === 'open') {
          $.mimiUI.offcanvas._hideCanvas();
        } else {
          $.mimiUI.offcanvas._showCanvas();
        }
      });
    },

    applyClosingHandler: function() {
      $('[data-close-offcanvas]').click(function() {
        $.mimiUI.offcanvas._hideCanvas();
      });
    },

    _hideCanvas: function() {
      $('body').attr('data-offcanvas', 'closed');
      $('[data-close-offcanvas]').hide();
      $('[data-offcanvas-menu]').hide();

      $('.mm-main').removeClass('mm-offcanvas--active');
      $('.mm-offcanvas').removeClass('mm-offcanvas--open');

      $('[data-offcanvas-overlay]').hide();
    },

    _showCanvas: function() {
      $('body').attr('data-offcanvas', 'open');
      $('.mm-offcanvas').addClass('mm-offcanvas--open');
      $('.mm-main').addClass('mm-offcanvas--active');
      $('[data-offcanvas-overlay]').show();
      $('[data-offcanvas-menu]').delay(300).fadeIn();
      $('[data-close-offcanvas]').delay(300).fadeIn();
    },

    addOverlayToDom: function() {
      var overlayDiv = '<div class="mm-offcanvas-overlay"' +
                       'data-offcanvas-overlay></div>';
      $('body').prepend(overlayDiv);
    },

    applyClosingHandlerToOverlay: function() {
      $('[data-offcanvas-overlay]').click(function() {
        $.mimiUI.offcanvas._hideCanvas();
      });
    },

    initialize: function() {
      this.addOverlayToDom();
      this.applyClickHandler();
      this.applyClosingHandler();
      this.applyClosingHandlerToOverlay();
    }
  };
})(jQuery);
