'use strict';
(function($) {
  $.mimiUI = $.mimiUI || {};
  $.mimiUI.select = {
    applySelect: function() {
      $('select').selectric();
    },
    /**
    * Initialize module
    */
    initialize: function() {
      this.applySelect();
    }
  };
})(jQuery);
