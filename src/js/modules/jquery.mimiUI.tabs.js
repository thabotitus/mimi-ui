'use strict';
(function($) {
  $.mimiUI = $.mimiUI || {};
  $.mimiUI.tabs = {
    /**
    * Check that tabs and content match.
    */
    checkForMatchingTabs: function() {
      var allTabs = $('.mm-tab');
      var allTabContent = $('.mm-tabs__content');
      if (allTabs.length !== allTabContent.length) {
        console.error('MIMI TABS: Number of Tabs does not' +
                      'match number of tab content blocks');
      }
    },
    /**
    * Add Click handler to tab
    */
    applyClickHandler: function() {
      var allTabs = $('.mm-tab');
      $.each(allTabs, function() {
        $(this).on('click', function() {
          $.mimiUI.tabs.deActivateCurrentTab($(this));
          $.mimiUI.tabs.activateTab($(this));
        });
      });
    },
    /**
    * De-Activate current active tab
    * @param {tab} tab that was clicked on
    */
    deActivateCurrentTab: function(tab) {
      var activeTabContent = tab.closest('.mm-tabs')
                                .find('.mm-tabs__content--active');
      var activeTab = tab.closest('.mm-tabs__menu').find('.mm-tab--active');
      activeTab.removeClass('mm-tab--active');
      activeTabContent.removeClass('mm-tabs__content--active');
    },
    /**
    * Activate current tab
    * @param {tab} tab that was clicked on
    */
    activateTab: function(tab) {
      var data = tab.data('tab');
      var matchingContent = $('.mm-tabs__content[data-tab=' + data + ']');
      tab.addClass('mm-tab--active');
      matchingContent.addClass('mm-tabs__content--active');
    },
    /**
    * Initialize module
    */
    initialize: function() {
      this.checkForMatchingTabs();
      this.applyClickHandler();
    }
  };
})(jQuery);
